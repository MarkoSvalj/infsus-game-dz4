import { Component } from '@angular/core';
import {CamundaRestService} from "../../services/camunda-rest.service";
import {Observable} from "rxjs";
import {AsyncPipe} from "@angular/common";
import {MatCard, MatCardContent} from "@angular/material/card";
import {MatButtonToggle} from "@angular/material/button-toggle";

@Component({
  selector: 'app-home',
  standalone: true,
  imports: [
    AsyncPipe,
    MatCardContent,
    MatCard,
    MatButtonToggle
  ],
  templateUrl: './home.component.html',
  styleUrl: './home.component.scss'
})
export class HomeComponent {

  processDefinitions$: Observable<any> = this.crs.getProcessDefinitions();

  constructor(private crs: CamundaRestService) {
    this.crs.getProcessDefinitions().subscribe(tasks => {
    });
  }

  start(key: string) {
    let variables = {
      "variables": {
        "author": {
          "value": "aStringValue",
          "type": "String"
        }
      }
    }

    this.crs.submitForm( key, variables
    ).subscribe(tasks => {
      console.log(tasks);
    });
  }
}
