export class ProcessDefinition {
  id: string;
  name: string;
  key: string;

  constructor(id: string, name: string, key: string) {
    this.id = id;
    this.name = name;
    this.key = key;
  }
}
