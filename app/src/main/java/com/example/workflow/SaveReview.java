package com.example.workflow;
import java.util.List;

import org.camunda.bpm.engine.delegate.BpmnError;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.beans.factory.annotation.Autowired;
import jakarta.inject.Named;

@Named
public class SaveReview implements JavaDelegate{

    @Autowired
    ReviewRepository reviewRepository;

    @Override
    public void execute(DelegateExecution delegateExecution) throws Exception {
        String author = (String) delegateExecution.getVariable("author");
        String title = (String) delegateExecution.getVariable("review_title");
        String content = (String) delegateExecution.getVariable("review_content");
        String videogame = (String) delegateExecution.getVariable("review_videogame");
        Integer rating = Integer.parseInt((String) delegateExecution.getVariable("review_rating"));

        List<Review> reviews = reviewRepository.findAll();
        for (Review review : reviews) {
            if (review.getVideogame().equals(videogame) && review.getAuthor().equals(author))
                throw new BpmnError("no_duplicate_reviews", "User already wrote a review for this videogame");
        }

        Review review = Review.builder().author(author).content(content).rating(rating).title(title).videogame(videogame).build();
        reviewRepository.save(review);
    }
    
}
