# Upute za pokretanje
1. radi jednostavnosti uz aplikaciju priložena je H2 baza podataka u koju su spremljeni korisnici "Korisnik" s lozinkom `korisnik`, "Admin" s lozinkom `admin` te ostale informacije i konfiguracijski parametri potrebni za ispravan rad Camunde. Dostupan je i super korisnik "demo" s lozinkom `demo`.
1. potrebno je koristeći terminal pozicionirati se unutar `app` direktorija
1. ovisno o OS-u korisnika potrebno je pokrenuti sljedeće naredbe:
	- Linux: `./mvnw clean install` te zatim `./mvnw spring-boot:run`
	- Windows: `./mvnw.cmd clean install` te zatim `./mvnw.cmd spring-boot:run`
